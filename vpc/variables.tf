provider "aws" {
  region     = "eu-west-1"
}

###################################  Terraform State file storage vars

terraform {
  backend "s3" {
    bucket = "gitlab-terraform" 
    region = "eu-west-1"
    encrypt = "true"
    dynamodb_table = "terraform-lock"
  }
}

###################################  Random String Vars

resource "random_string" "random" {
  length = 4
  special = false
  upper = false
  number = false
}

###################### Global Tag Vars

variable "tf_global_tags" {
  description = "A map of tags to add to all resources"
  type = map(string)
}

################## VPC Module vars #################

variable "tf_cidr" {
  description = "The CIDR block for the VPC"
  default = "10.229.112.0/23"
}

variable "tf_azs" {
  description = "A list of availability zones in the   region"
  default = ["eu-west-1a", "eu-west-1b"]
}

variable "tf_prvsubnts" {
  description = "A list of private subnets inside the VPC"
  default = ["10.229.113.0/25", "10.229.113.128/25"]
}

variable "tf_pubsubnts" {
  description = "A list of public subnets inside the VPC"
  default = ["10.229.112.0/27", "10.229.112.32/27"]
}


################## AWS SG Module vars #################

variable "vpc_cidr_block_sg" {
  description = "Enter VPC CIDR Block"
  default     = "10.0.0.0/8"
}

################## AWS ACM Module vars #################

variable "route53_domain_name" {
  description = "Name of the Domain"
  default     = "abc.office.com"
}

variable "route53_zone_id" {
  description = "Name of the Domain"
  default     = "AAAAAAAAA"
}

variable "tools_domain_name_1" {
  description = "Name of the DNS Record for the tools Internal ALB"
  default     = "pact-broker.abc.office.com"
}

variable "tools_domain_name_2" {
  description = "Name of the DNS Record for the tools Internal ALB"
  default     = "radiator.abc.office.com"
}

variable "domain_name_1" {
  description = "Name of the Domain"
  default     = "qa101.abc.office.com"
}

variable "domain_name_2" {
  description = "Name of the Domain"
  default     = "qa202.abc.office.com"
}

variable "domain_name_3" {
  description = "Name of the Domain"
  default     = "qa303.abc.office.com"
}

variable "domain_name_4" {
  description = "Name of the Domain"
  default     = "qa404.abc.office.com"
}

variable "domain_name_5" {
  description = "Name of the Domain"
  default     = "qa505.abc.office.com"
}

variable "domain_name_6" {
  description = "Name of the Domain"
  default     = "qa606.abc.office.com"
}

variable "domain_name_7" {
  description = "Name of the Domain"
  default     = "qa707.abc.office.com"
}

variable "domain_name_8" {
  description = "Name of the Domain"
  default     = "qa808.abc.office.com"
}

variable "domain_name_9" {
  description = "Name of the Domain"
  default     = "qa909.abc.office.com"
}

variable "domain_name_10" {
  description = "Name of the Domain"
  default     = "qa110.abc.office.com"
}

################## AWS Application Load Balancer vars #################
variable "alb_s3_name" {
  description = "Name of the S3 bucket"
  default     = "edm-dev-logs"
}

variable "pact_port" {
  description = "The port that Pact Broker runs on"
  default     = "9292"
}

variable "pact_protocol" {
  description = "The protocol that Pact Broker runs on"
  default     = "HTTP"
}
