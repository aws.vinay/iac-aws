
resource "aws_alb" "tools_internal" {
	name	    	    =	"${var.tf_global_tags["Environment"]}-alb-int-${random_string.random.id}"
	depends_on      = [module.vpc.private_subnets]
	internal	      =	true
	security_groups	=	[aws_security_group.private_web.id]
  subnets		      =	[module.vpc.private_subnets[0], module.vpc.private_subnets[1]]

  access_logs {    
    bucket = var.alb_s3_name
    prefix = "loadbalancers"
    enabled = true  
  }
  
  tags = var.tf_global_tags
}

resource "aws_route53_record" "pact_broker" {
  name    = var.tools_domain_name_1
  type    = "A"
  zone_id = data.aws_route53_zone.external.zone_id

  alias {
    name                   = aws_alb.tools_internal.dns_name
    zone_id                = aws_alb.tools_internal.zone_id
    evaluate_target_health = true
  }
}

resource "aws_alb" "web_front" {
	name		=	"${var.tf_global_tags["Environment"]}-alb-${random_string.random.id}"
	depends_on = [module.vpc.public_subnets]
	internal	=	false
	security_groups	=	[aws_security_group.testing_whitelist.id, aws_security_group.private.id]
  subnets		=	[module.vpc.public_subnets[0], module.vpc.public_subnets[1]]
  access_logs {    
    bucket = var.alb_s3_name
    prefix = "loadbalancers"
    enabled = true  
  }
  
	#enable_deletion_protection	=	true
  tags = var.tf_global_tags
}

resource "aws_lb_target_group" "pact" {
	name        = "${var.tf_global_tags["Environment"]}-pact-${random_string.random.id}"
	depends_on  = [module.vpc.private_subnets]
	vpc_id	    = module.vpc.vpc_id
	port	      = var.pact_port
	protocol	  = var.pact_protocol
  target_type = "ip"

	health_check {
    path = "/"
    healthy_threshold = 2
    unhealthy_threshold = 10
    interval = 300
    timeout = 60
    matcher = "200,301,302"
  }
  
  tags = var.tf_global_tags

}

resource "aws_alb_target_group" "front_end" {
	name = "${var.tf_global_tags["Environment"]}-${random_string.random.id}"
	depends_on = [module.vpc.public_subnets]
	vpc_id	= module.vpc.vpc_id
	port	= "80"
	protocol	= "HTTP"
	health_check {
                path = "/"
                #port = "80"
                #protocol = "HTTP"
                healthy_threshold = 2
                unhealthy_threshold = 10
                interval = 300
                timeout = 60
                matcher = "200,301,302"
        }
    tags = var.tf_global_tags

}

resource "aws_alb_listener" "tools_internal" {
  load_balancer_arn = aws_alb.tools_internal.arn
  port              = "443"
  protocol          = "HTTPS"
  certificate_arn   = aws_acm_certificate.cert_tools.arn
  ssl_policy        = "ELBSecurityPolicy-TLS-1-2-Ext-2018-06"
  
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.pact.arn
  }
}

resource "aws_alb_listener" "front_end" {
  load_balancer_arn = aws_alb.web_front.arn
  port              = "443"
  protocol          = "HTTPS"
#  ssl_policy        = "ELBSecurityPolicy-2016-08"
  #certificate_arn  = module.acm.this_acm_certificate_arn
  certificate_arn   = aws_acm_certificate.cert.arn
  ssl_policy        = "ELBSecurityPolicy-TLS-1-2-2017-01"
  
  
  default_action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.front_end.arn
  }
}

############################# AWS ALB DNS Name
output "alb_tools_internal_dns_output" {
  value = aws_alb.tools_internal.dns_name
}

output "alb_tools_internal_dns_output_arn" {
  value = aws_alb.tools_internal.id
}

output "alb_dns_output" {
  value = aws_alb.web_front.dns_name
}

output "alb_dns_output_arn" {
  value = aws_alb.web_front.id
}
