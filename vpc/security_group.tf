
resource "aws_security_group" "private" {
    name = "Internal"
    description = "IHS VPC CIDR Block-Private"

	ingress {
		from_port = 0
        to_port = 65535
        protocol = "TCP"
        cidr_blocks = [var.vpc_cidr_block_sg]
		description = "IHS Private CIDR Block"
    }
    egress { 
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
		description = "outbound to Interent"
    }
    tags = merge(
        map(
            "Name","Internal-${var.tf_global_tags["Environment"]}-${random_string.random.id}"
        ),
        var.tf_global_tags
    )

  vpc_id = module.vpc.vpc_id
}



resource "aws_security_group" "private_web" {
    name = "private_web"
    description = "Private Networks"

	ingress {
		from_port = 443
        to_port = 443
        protocol = "TCP"
        cidr_blocks = [var.vpc_cidr_block_sg]
		description = "CIDR Block"
    }
    egress { 
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
		description = "outbound to Interent"
    }
    tags = merge(
        { "Name" = "Internal-Web-${var.tf_global_tags["Environment"]}-${random_string.random.id}" },
        var.tf_global_tags
    )

  vpc_id = module.vpc.vpc_id
}