###################################  Random String Values

output "output_random_string_values" {
  value = random_string.random.id
}

output "output_var_map_values" {
  value = var.tf_global_tags["Environment"]
}

###################################  VPC Output

output "vpc_id" {
  description = "The ID of the VPC"
  value       = module.vpc.vpc_id
}

output "prv_sub_ids_0" {
  description = "List of IDs of private subnets"
  value       = module.vpc.private_subnets[0]
}

output "prv_sub_ids_1" {
  description = "List of IDs of private subnets"
  value       = module.vpc.private_subnets[1]
}

output "pub_sub_ids_0" {
  description = "List of IDs of private subnets"
  value       = module.vpc.public_subnets[0]
}

output "pub_sub_ids_1" {
  description = "List of IDs of private subnets"
  value       = module.vpc.public_subnets[1]
}

############################# Security Group Output

output "output_security_group_private_web" {
  value = aws_security_group.private_web.id
}

output "output_security_group_private" {
  value = aws_security_group.private.id
}

output "output_security_group_ihsmarkit_whitelist" {
  value = aws_security_group.ihsmarkit_whitelist.id
}

output "output_security_group_luxoft" {
  value = aws_security_group.luxoft.id
}



