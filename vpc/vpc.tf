module "vpc" {
  source = "terraform-aws-modules/vpc/aws"
  version = "2.24.0"

  name = "${var.tf_global_tags["Environment"]}-VPC-${random_string.random.id}"
  cidr = var.tf_cidr
  azs             = var.tf_azs
  private_subnets = var.tf_prvsubnts
  public_subnets  = var.tf_pubsubnts
  enable_nat_gateway = true

  tags = var.tf_global_tags
}

module "vpc-flow-logs" {
  source  = "umotif-public/vpc-flow-logs/aws"
  version = "1.0.1"
  vpc_id = "${module.vpc.vpc_id}"
  name_prefix = "${var.tf_global_tags["Environment"]}-VPC-${random_string.random.id}"
}