
########################### AWS ACM SSL Certificate creation

resource "aws_acm_certificate" "cert_tools" {
  domain_name               = var.tools_domain_name_1
  subject_alternative_names = [var.tools_domain_name_2]
  validation_method         = "DNS"  

  tags = merge(
    {"Name" = "${var.tf_global_tags["Environment"]}-acm-tools-${random_string.random.id}"},
    var.tf_global_tags
  )

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_acm_certificate" "cert" {
  domain_name       = "${random_string.random.id}-${var.domain_name_1}"
  subject_alternative_names = ["${random_string.random.id}-${var.domain_name_2}","${random_string.random.id}-${var.domain_name_3}","${random_string.random.id}-${var.domain_name_4}","${random_string.random.id}-${var.domain_name_5}","${random_string.random.id}-${var.domain_name_6}","${random_string.random.id}-${var.domain_name_7}","${random_string.random.id}-${var.domain_name_8}","${random_string.random.id}-${var.domain_name_9}","${random_string.random.id}-${var.domain_name_10}"]
  validation_method = "DNS"  
  tags = merge(
       map(
           "Name","${var.tf_global_tags["Environment"]}-acm-${random_string.random.id}"
       ),
       var.tf_global_tags
  )

  lifecycle {
    create_before_destroy = true
  }
}

data "aws_route53_zone" "external" {
  name = var.route53_domain_name
}

resource "aws_route53_record" "validation_tools_0" {
  name    = aws_acm_certificate.cert_tools.domain_validation_options.0.resource_record_name
  type    = aws_acm_certificate.cert_tools.domain_validation_options.0.resource_record_type
  zone_id = data.aws_route53_zone.external.zone_id
  records = [aws_acm_certificate.cert_tools.domain_validation_options.0.resource_record_value]
  ttl     = "45"
}

resource "aws_route53_record" "validation_tools_1" {
  name    = aws_acm_certificate.cert_tools.domain_validation_options.1.resource_record_name
  type    = aws_acm_certificate.cert_tools.domain_validation_options.1.resource_record_type
  zone_id = data.aws_route53_zone.external.zone_id
  records = [aws_acm_certificate.cert_tools.domain_validation_options.1.resource_record_value]
  ttl     = "45"
}

resource "aws_route53_record" "validation_0" {
  name    = aws_acm_certificate.cert.domain_validation_options.0.resource_record_name
  type    = aws_acm_certificate.cert.domain_validation_options.0.resource_record_type
  zone_id = data.aws_route53_zone.external.zone_id
  records = ["${aws_acm_certificate.cert.domain_validation_options.0.resource_record_value}"]
  ttl     = "45"
}

resource "aws_route53_record" "validation_1" {
  name    = aws_acm_certificate.cert.domain_validation_options.1.resource_record_name
  type    = aws_acm_certificate.cert.domain_validation_options.1.resource_record_type
  zone_id = data.aws_route53_zone.external.zone_id
  records = ["${aws_acm_certificate.cert.domain_validation_options.1.resource_record_value}"]
  ttl     = "45"
}

resource "aws_route53_record" "validation_2" {
  name    = aws_acm_certificate.cert.domain_validation_options.2.resource_record_name
  type    = aws_acm_certificate.cert.domain_validation_options.2.resource_record_type
  zone_id = data.aws_route53_zone.external.zone_id
  records = ["${aws_acm_certificate.cert.domain_validation_options.2.resource_record_value}"]
  ttl     = "45"
}

resource "aws_route53_record" "validation_3" {
  name    = aws_acm_certificate.cert.domain_validation_options.3.resource_record_name
  type    = aws_acm_certificate.cert.domain_validation_options.3.resource_record_type
  zone_id = data.aws_route53_zone.external.zone_id
  records = ["${aws_acm_certificate.cert.domain_validation_options.3.resource_record_value}"]
  ttl     = "45"
}

resource "aws_route53_record" "validation_4" {
  name    = aws_acm_certificate.cert.domain_validation_options.4.resource_record_name
  type    = aws_acm_certificate.cert.domain_validation_options.4.resource_record_type
  zone_id = data.aws_route53_zone.external.zone_id
  records = ["${aws_acm_certificate.cert.domain_validation_options.4.resource_record_value}"]
  ttl     = "45"
}

resource "aws_route53_record" "validation_5" {
  name    = aws_acm_certificate.cert.domain_validation_options.5.resource_record_name
  type    = aws_acm_certificate.cert.domain_validation_options.5.resource_record_type
  zone_id = data.aws_route53_zone.external.zone_id
  records = ["${aws_acm_certificate.cert.domain_validation_options.5.resource_record_value}"]
  ttl     = "45"
}

resource "aws_route53_record" "validation_6" {
  name    = aws_acm_certificate.cert.domain_validation_options.6.resource_record_name
  type    = aws_acm_certificate.cert.domain_validation_options.6.resource_record_type
  zone_id = data.aws_route53_zone.external.zone_id
  records = ["${aws_acm_certificate.cert.domain_validation_options.6.resource_record_value}"]
  ttl     = "45"
}

resource "aws_route53_record" "validation_7" {
  name    = aws_acm_certificate.cert.domain_validation_options.7.resource_record_name
  type    = aws_acm_certificate.cert.domain_validation_options.7.resource_record_type
  zone_id = data.aws_route53_zone.external.zone_id
  records = ["${aws_acm_certificate.cert.domain_validation_options.7.resource_record_value}"]
  ttl     = "45"
}

resource "aws_route53_record" "validation_8" {
  name    = aws_acm_certificate.cert.domain_validation_options.8.resource_record_name
  type    = aws_acm_certificate.cert.domain_validation_options.8.resource_record_type
  zone_id = data.aws_route53_zone.external.zone_id
  records = ["${aws_acm_certificate.cert.domain_validation_options.8.resource_record_value}"]
  ttl     = "45"
}

resource "aws_route53_record" "validation_9" {
  name    = aws_acm_certificate.cert.domain_validation_options.9.resource_record_name
  type    = aws_acm_certificate.cert.domain_validation_options.9.resource_record_type
  zone_id = data.aws_route53_zone.external.zone_id
  records = ["${aws_acm_certificate.cert.domain_validation_options.9.resource_record_value}"]
  ttl     = "45"
}

resource "aws_acm_certificate_validation" "tools_internal" {
  certificate_arn = aws_acm_certificate.cert_tools.arn
  validation_record_fqdns = [
    aws_route53_record.validation_tools_0.fqdn,
    aws_route53_record.validation_tools_1.fqdn
  ]
}

resource "aws_acm_certificate_validation" "default" {
  certificate_arn = aws_acm_certificate.cert.arn
  validation_record_fqdns = [
  "${aws_route53_record.validation_0.fqdn}",
  "${aws_route53_record.validation_1.fqdn}",
  "${aws_route53_record.validation_2.fqdn}",
  "${aws_route53_record.validation_3.fqdn}",
  "${aws_route53_record.validation_4.fqdn}",
  "${aws_route53_record.validation_5.fqdn}",
  "${aws_route53_record.validation_6.fqdn}",
  "${aws_route53_record.validation_7.fqdn}",
  "${aws_route53_record.validation_8.fqdn}",
  "${aws_route53_record.validation_9.fqdn}",  
  ]
}

############################# AWS ACM Certificate arn
output "output_acm_tools_certificate_arn" {
  description = "The ARN of the tools certificate"
  value       = aws_acm_certificate.cert_tools.arn  
}

output "output_acm_certificate_arn" {
  description = "The ARN of the certificate"
  value       = aws_acm_certificate.cert.arn  
}