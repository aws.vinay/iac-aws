data "aws_ami" "aws_windows_ami" {
  most_recent = true
  owners = ["amazon"]

  filter {
    name   = "name"
    values = ["Windows_Server-2016-English-Full-ECS_Optimized-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  
}


resource "aws_launch_configuration" "nodes" {
  associate_public_ip_address = "${var.auto_assign_publicip}"
  iam_instance_profile        = "${aws_iam_instance_profile.ecs_instance_profile.name}"
  image_id                    = "${data.aws_ami.aws_windows_ami.id}"
  instance_type               = "${var.instance-type}"
  name 						  = "${upper(var.ecs_cluster_name)}-ECS-AUTOSCALING"
  security_groups             = ["${var.sg-id}"]
  key_name 					  = "${var.key_name}"
  
  user_data = <<EOF
  <powershell>
    Set-TimeZone "Eastern Standard Time"
	Import-Module ECSTools
	Initialize-ECSAgent -Cluster '${var.ecs_cluster_name}' -EnableTaskIAMRole
  </powershell>
  EOF
  
  root_block_device = [ {
   volume_size = "${var.root_disk_size}"
   volume_type = "standard"
} ]

  lifecycle {
    create_before_destroy = true
  }
}


resource "aws_autoscaling_group" "as-group" {
  desired_capacity     = "${var.min-size}"
  launch_configuration = "${aws_launch_configuration.nodes.id}"
  max_size             = "${var.max-size}"
  min_size             = "${var.min-size}"
  name                 = "${upper(var.ecs_cluster_name)}-ECS-AUTOSCALING"
  vpc_zone_identifier  = ["${var.subnet-id}"]

  tag {
    key                 = "Name"
    value               = "AS-${upper(var.server_name)}"
    propagate_at_launch = true
  }

  tag {
    key                 = "ENVIRONMENT"
    value               = "${upper(var.server_environment)}"
    propagate_at_launch = true
  }

}
