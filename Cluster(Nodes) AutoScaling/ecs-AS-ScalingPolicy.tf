resource "aws_autoscaling_policy" "ecs-increase" {
name = "ecs-increase"
scaling_adjustment = 1
policy_type = "SimpleScaling"
adjustment_type = "ChangeInCapacity"
cooldown = "${var.cooldown-period}"
autoscaling_group_name = "${aws_autoscaling_group.as-group.name}"
}


resource "aws_autoscaling_policy" "ecs-decrease" {
name = "ecs-decrease"
scaling_adjustment = -1
policy_type = "SimpleScaling"
adjustment_type = "ChangeInCapacity"
cooldown = "${var.cooldown-period}"
autoscaling_group_name = "${aws_autoscaling_group.as-group.name}"
}