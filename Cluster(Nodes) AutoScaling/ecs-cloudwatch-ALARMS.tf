

resource "aws_cloudwatch_metric_alarm" "cpu-high" {
alarm_name = "${var.ecs_cluster_name}-ECS-High_CPUResv"
comparison_operator = "GreaterThanOrEqualToThreshold"
evaluation_periods = "1"
datapoints_to_alarm = 1
metric_name = "CPUReservation"
namespace = "AWS/ECS"
period = "60"
statistic = "Maximum"
threshold = "${var.cpu-high-threshold}"

dimensions = {
    ClusterName = "${var.ecs_cluster_name}"
}

alarm_description = " Cluster CPU reservation above ${var.cpu-high-threshold}%"
alarm_actions = ["${aws_autoscaling_policy.ecs-increase.arn}"]
}



resource "aws_cloudwatch_metric_alarm" "cpu-low" {
alarm_name = "${var.ecs_cluster_name}-ECS-Low_CPUResv"
comparison_operator = "LessThanThreshold"
evaluation_periods = "1"
datapoints_to_alarm = 1
metric_name = "CPUReservation"
namespace = "AWS/ECS"
period = "60"
statistic = "Maximum"
threshold = "${var.cpu-low-threshold}"

dimensions = {
    ClusterName = "${var.ecs_cluster_name}"
}

alarm_description = " Cluster CPU reservation is below ${var.cpu-low-threshold}%"
alarm_actions = ["${aws_autoscaling_policy.ecs-decrease.arn}"]
}


resource "aws_cloudwatch_metric_alarm" "memory-high" {
alarm_name = "${var.ecs_cluster_name}-ECS-High_MemResv"
comparison_operator = "GreaterThanOrEqualToThreshold"
evaluation_periods = "1"
datapoints_to_alarm = 1
metric_name = "MemoryReservation"
namespace = "AWS/ECS"
period = "60"
statistic = "Maximum"
threshold = "${var.memory-high-threshold}"

dimensions = {
    ClusterName = "${var.ecs_cluster_name}"
}

alarm_description = " Cluster Memmory reservation above ${var.memory-high-threshold}%"
alarm_actions = ["${aws_autoscaling_policy.ecs-increase.arn}"]
}

resource "aws_cloudwatch_metric_alarm" "memory-low" {
alarm_name = "${var.ecs_cluster_name}-ECS-Low_MemResv"
comparison_operator = "LessThanThreshold"
evaluation_periods = "1"
datapoints_to_alarm = 1
metric_name = "MemoryReservation"
namespace = "AWS/ECS"
period = "60"
statistic = "Maximum"
threshold = "${var.memory-low-threshold}"

dimensions = {
    ClusterName = "${var.ecs_cluster_name}"
}

alarm_description = " Cluster Memory reservation is below ${var.memory-low-threshold}%"
alarm_actions = ["${aws_autoscaling_policy.ecs-decrease.arn}"]
}
