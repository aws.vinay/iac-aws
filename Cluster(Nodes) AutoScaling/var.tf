####################### ECS Cluster configuration #####################
provider "aws" {
  #access_key = "AAAAAAAAAAAAAA"
  #secret_key = "AAAAAAAAAAAA"
  region  = "eu-west-1"
}
variable "ecs_cluster_name" {
  description = "Name of the ECS Cluster"
  default     = "assignment"
}

############################## AS Configuration #####################

variable "private_key_path" {}
variable "key_name" {
  default = "assignment"
}

variable "server_name" {
#name of the computer is limited to 15 bytes, which is 15 characters in this case
  description = "Machine Name"
  default = "IRLI-AWSECS"
}

variable "server_environment" {
  description = "Environment of the Server"
  default = "Dev"
}

variable "instance-type" {
  description = "EC2 machine type"
  default = "t3.medium"
  type    = "string"
}


variable "min-size" {
  description = "Mention Minimum Group Size"
  default = "1"
  type    = "string"
}
  
variable "max-size" {
  description = "Mention Maximum Group Size"
  default = "5"
  type    = "string"
}

  
variable "sg-id" {
  description = "Mention 2 Security Group ID's"
  default = ["sg-0c63eb14e3bcdc70a","sg-0df25c0934cd3a88f"]
  type    = "list"
}

variable "root_disk_size" {
  description = "The allocated storage in gibibytes."
  default     = "150"
}


variable "auto_assign_publicip" {
  description = "auto_assign_publicip to EC2 Machine"
#  default     = "true"
 default     = "false"  
}

variable "subnet-id" {
  description = "Mention 2 Subnet ID's"
  default = ["subnet-0cc1c3d1d05c9656f","subnet-0eecc7f44bbddcc5f"]
  type    = "list"
}

variable "cpu-high-threshold" {
  default     = "75"
}

variable "cpu-low-threshold" {
  default     = "25"
}

variable "memory-high-threshold" {
  default     = "75"
}

variable "memory-low-threshold" {
  default     = "25"
}

variable "cooldown-period" {
  description = "The cooldown period helps to ensure that your Auto Scaling group doesn't launch or terminate additional instances before the previous scaling activity takes effect"
  default     = "900"
}