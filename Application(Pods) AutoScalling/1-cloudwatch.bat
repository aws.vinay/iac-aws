setlocal EnableDelayedExpansion

@rem setting the two most common paths for msbuild installation, one test coming from BuildTools2017 and one coming from VisualStudio 2017
SET DEFAULTMSBUILDFROMBUILDTOOLS_PATH="C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\MSBuild\15.0\Bin"

SET FOLDER_NAME=DEV

set CLUSTER_NAME=myefdev
set IMAGE_NAME=hostfamilyapi
set Itask_definition=%IMAGE_NAME%
set ELB_TARGET=arn:aws:elasticloadbalancing:eu-west-1:237279346256:targetgroup/hostfamilydev-api/22c6f9d71eb10716
set CONTAINER_PORT=83
set IAM_ROLE=container-myef-hostfamily-webapi-role
set revision=2

:: The value of the AUTO_SCALING variable can be YES or NO or DELETE.
:: YES indicates configure auto scaling 
:: DELETE indicates delete the autoscaling configuration if exist.
:: NO indicates nothing to do  
:: YES or DELETE or NO

set AUTO_SCALING=YES
::set AUTO_SCALING=DELETE

set MIN_CAPACITY=1
set MAX_CAPACITY=2

set THRESHOLD=75


aws application-autoscaling put-scaling-policy --service-namespace ecs --scalable-dimension ecs:service:DesiredCount --resource-id service/%CLUSTER_NAME%/%IMAGE_NAME% --policy-name my-step-scaling-in-policy --policy-type StepScaling --step-scaling-policy-configuration file://scale-in-policy-config.json


powershell.exe aws application-autoscaling describe-scaling-policies --service-namespace ecs --query "ScalingPolicies[0].PolicyARN" > policy_arn.txt

SET /p POLICY_ARN=<policy_arn.txt

echo %POLICY_ARN%

aws cloudwatch put-metric-alarm --alarm-name Step-Scaling-AlarmHigh-ECS:service/%CLUSTER_NAME%/%IMAGE_NAME% --metric-name CPUUtilization --namespace AWS/ECS --statistic Average --period 300 --threshold %THRESHOLD% --comparison-operator GreaterThanThreshold  --dimensions "Name=ServiceName,Value=%IMAGE_NAME%" "Name=ClusterName,Value=%CLUSTER_NAME%" --evaluation-periods 2 --alarm-actions %POLICY_ARN%

IF EXIST policy_arn.txt DEL /F policy_arn.txt