
powershell.exe aws application-autoscaling describe-scaling-policies --service-namespace ecs --query "ScalingPolicies[0].PolicyARN" > policy_arn.txt

SET /p POLICY_ARN=<policy_arn.txt

echo %POLICY_ARN%

aws cloudwatch put-metric-alarm --alarm-name Step-Scaling-AlarmHigh-ECS:service/%CLUSTER_NAME%/%IMAGE_NAME% --metric-name CPUUtilization --namespace AWS/ECS --statistic Average --period 300 --threshold %THRESHOLD% --comparison-operator GreaterThanThreshold  --dimensions "Name=ServiceName,Value=%IMAGE_NAME%" "Name=ClusterName,Value=%CLUSTER_NAME%" --evaluation-periods 2 --alarm-actions %POLICY_ARN%

IF EXIST policy_arn.txt DEL /F policy_arn.txt