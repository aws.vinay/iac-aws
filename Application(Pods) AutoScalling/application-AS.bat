setlocal EnableDelayedExpansion

@rem setting the two most common paths for msbuild installation, one test coming from BuildTools2017 and one coming from VisualStudio 2017
SET DEFAULTMSBUILDFROMBUILDTOOLS_PATH="C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\MSBuild\15.0\Bin"

SET FOLDER_NAME=DEV

set CLUSTER_NAME=myefdev
set IMAGE_NAME=hostfamilyapi
set Itask_definition=%IMAGE_NAME%
set ELB_TARGET=arn:aws:elasticloadbalancing:eu-west-1:123456675432:targetgroup/hostfamilydev-api/22c6f9d71eb10716
set CONTAINER_PORT=83
set IAM_ROLE=container-myef-hostfamily-webapi-role
set revision=2

:: The value of the AUTO_SCALING variable can be YES or NO or DELETE.
:: YES indicates configure auto scaling 
:: DELETE indicates delete the autoscaling configuration if exist.
:: NO indicates nothing to do  
:: YES or DELETE or NO

set AUTO_SCALING=YES
::set AUTO_SCALING=DELETE

set MIN_CAPACITY=1
set MAX_CAPACITY=2

set THRESHOLD=75



:: AWS ECS Service or Application Auto Scaling
IF /I "%AUTO_SCALING%"=="YES" (
	echo Setting up Auto Scale.

		aws application-autoscaling describe-scalable-targets --service-namespace ecs  --query "ScalableTargets[].ResourceId" --output text > scalable_targets.txt
		findstr /c:"service/%CLUSTER_NAME%/%IMAGE_NAME%" "scalable_targets.txt" >nul 2>&1
		if not errorlevel 1 (
				echo "Autoscaling is already enabled, nothing to do"
	   
		) else (	
				echo "Autoscaling is not enabled"				
				aws application-autoscaling register-scalable-target --service-namespace ecs --scalable-dimension ecs:service:DesiredCount --resource-id service/%CLUSTER_NAME%/%IMAGE_NAME% --min-capacity %MIN_CAPACITY% --max-capacity %MAX_CAPACITY%
				aws application-autoscaling put-scaling-policy --service-namespace ecs --scalable-dimension ecs:service:DesiredCount --resource-id service/%CLUSTER_NAME%/%IMAGE_NAME% --policy-name my-step-scaling-policy --policy-type StepScaling --step-scaling-policy-configuration file://scale-out-policy-config.json
				TIMEOUT 10				
				CALL cloudwatch.bat							
				IF EXIST policy_arn.txt DEL /F policy_arn.txt
				echo "Autoscaling is successfully enabled"		
				IF EXIST scalable_targets.txt DEL /F scalable_targets.txt )
	) ELSE IF /I "%AUTO_SCALING%"=="DELETE" (
		echo "Deleting Autoscaling if exist"
		aws application-autoscaling describe-scalable-targets --service-namespace ecs  --query "ScalableTargets[].ResourceId" --output text > scalable_targets.txt
		findstr /c:"service/%CLUSTER_NAME%/%IMAGE_NAME%" "scalable_targets.txt" >nul 2>&1
		if not errorlevel 1 (
		aws cloudwatch delete-alarms --alarm-name Step-Scaling-AlarmHigh-ECS:service/%CLUSTER_NAME%/%IMAGE_NAME%
		
		aws application-autoscaling deregister-scalable-target --service-namespace ecs --scalable-dimension ecs:service:DesiredCount --resource-id service/%CLUSTER_NAME%/%IMAGE_NAME%
		
		IF EXIST scalable_targets.txt DEL /F scalable_targets.txt
		echo "Autoscaling Successfully deleted"
		
		) else (
				echo "Unable to find the Autoscaling Service"
		)		
		
) ELSE IF /I "%AUTO_SCALING%"=="NO" (

		echo Auto scaling is not enabled 

)